#!/bin/sh

git remote update -p >/dev/null

locale=$(git log upstream --oneline|head -1|cut -d' ' -f1)
remote=$(git log origin/upstream --oneline|head -1|cut -d' ' -f1)
actual_branch=$(git branch|grep '*'|cut -d' ' -f2)

while true; do
    clear
    line="
    *****************************************
    *****************************************
    **    █████▒██▓     ▒█████   █     █░  **
    **  ▓██   ▒▓██▒    ▒██▒  ██▒▓█░ █ ░█░  **
    **  ▒████ ░▒██░    ▒██░  ██▒▒█░ █ ░█   **
    **  ░▓█▒  ░▒██░    ▒██   ██░░█░ █ ░█   **
    **  ░▒█░   ░██████▒░ ████▓▒░░░██▒██▓   **
    **   ▒ ░   ░ ▒░▓  ░░ ▒░▒░▒░ ░ ▓░▒ ▒    **
    **   ░     ░ ░ ▒  ░  ░ ▒ ▒░   ▒ ░ ░    **
    **   ░ ░     ░ ░   ░ ░ ░ ▒    ░   ░    **
    **             ░  ░    ░ ░      ░      **
    **                                     **
    *****************************************
    *****************************************
    "
    echo -e " \033[0;34m $line  \033[0m";
    if [ "$locale" != "$remote" ]; then
        echo -e "\033[33;31m[1] Update your repository\033[0m"
    else
        echo -e "[1] Update your repository"
    fi
        echo -e "[2] Create feature branch"
        echo -e "[3] Generate and Apply patch"
        echo -e "[4] Update upstream"
        echo -e "[5] Finalize feature"
        echo -e "[6] Exit"

    read -p "Please enter your choice: " yn
    case $yn in
        [1]* )
            git checkout upstream
            git pull --rebase
            git checkout $actual_branch
            break;;
        [2]* )
            while true; do
                read -p "Enter a feature name (separated by underline): " branch
                if [ "$branch" != "" ]; then
                    echo $branch
                    git checkout upstream
                    git remote update -p
                    git pull --rebase
                    git checkout -b $branch
                    exit
                fi
            done
            break;;
        [3]* )
            git checkout upstream
            git pull --rebase
            git checkout $actual_branch
            git rebase upstream
            patch_name="${patch_name}.patch"
            git format-patch upstream > patches/$patch_name
            git checkout upstream
            git apply --stat patches/$patch_name
            git apply --check patches/$patch_name
            git am --signof < git apply --check patches/$patch_name
            git push origin upstream
            break;;
        [4]* )
            git push origin upstream
            break;;
        [5]* )
            git checkout upstream
            git pull --rebase
            git checkout $actual_branch
            git rebase upstream
            git push origin $actual_branch --force
            git checkout upstream
            git rebase $actual_branch
            git push origin upstream
            break;;
        [6]* )
            exit
            break;;
        * ) 
            echo "Please answer yes or no.";;
    esac
done
